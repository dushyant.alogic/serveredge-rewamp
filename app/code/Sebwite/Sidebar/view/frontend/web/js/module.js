/* ==========================================================================
 Scripts voor de frontend
 ========================================================================== */
require(['jquery'], function ($) {
    $(function () {
        $('.sidebar').on('click', '.o-list .expand, .o-list .expanded', function () {
            var element = $(this).parent('li');

            if (element.hasClass('active')) {
                element.find('ul').slideUp();

                element.removeClass('active');
                element.find('li').removeClass('active');

                element.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            } else {
                element.children('ul').slideDown();
                element.siblings('li').children('ul').slideUp();
                element.parent('ul').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
                element.find('> span i').removeClass('fa-angle-down').addClass('fa-angle-up');

                element.addClass('active');
                element.siblings('li').removeClass('active');
                element.siblings('li').find('li').removeClass('active');
                element.siblings('li').find('ul').slideUp();
            }
        });

        
        //$( ".sidebar-content ul" ).find('ul').slideUp();
        $( ".sidebar-content ul" ).find('li').each(function () {
  
            // "this" is the current child
            // in the loop grabbing this 
            // element in the form of string
            // and appending it to the 
            // "#output-container" div
            var element = $(this);
            if(!element.hasClass('active')){
                element.find('ul').slideUp();
            }
            console.log(element.find('ul'));
        });
    });
});
