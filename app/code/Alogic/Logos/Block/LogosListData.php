<?php
/**
 * @category   Alogic
 * @package    Alogic_Logos
 * @author     dushyantjoshia@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Logos\Block;

use Alogic\Logos\Model\LogosFactory;
use Magento\Framework\View\Element\Template\Context;

/**
 * Logos List block
 */
class LogosListData extends \Magento\Framework\View\Element\Template {
	/**
	 * @var Logos
	 */
	protected $_logos;
	public function __construct(
		Context $context,
		LogosFactory $logos
	) {
		$this->_logos = $logos;
		parent::__construct($context);
	}

	public function _prepareLayout() {
		$this->pageConfig->getTitle()->set(__('Alogic Logos Module List Page'));

		if ($this->getLogosCollection()) {
			$pager = $this->getLayout()->createBlock(
				'Magento\Theme\Block\Html\Pager',
				'alogic.logos.pager'
			)->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
				$this->getLogosCollection()
			);
			$this->setChild('pager', $pager);
			$this->getLogosCollection()->load();
		}
		return parent::_prepareLayout();
	}

	public function getLogosCollection() {
		$page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
		$pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

		$logos = $this->_logos->create();
		$collection = $logos->getCollection();
		$collection->addFieldToFilter('status', '1');
		//$logos->setOrder('logos_id','ASC');
		/*$collection->setPageSize($pageSize);
		$collection->setCurPage($page);*/

		return $collection;
	}

	public function getPagerHtml() {
		return $this->getChildHtml('pager');
	}
}