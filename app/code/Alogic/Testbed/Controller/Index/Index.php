<?php
namespace Alogic\Testbed\Controller\Index;
class Index extends \Magento\Framework\App\Action\Action {
	/**
	 * @var \Magento\Framework\Filesystem\Driver\File
	 */
	protected $file;
	/**
	 * @var \Magento\Framework\File\Csv
	 */
	protected $csv;
	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger;
	protected $resultPageFactory;

	protected $storeManager;

	protected $categoryFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Framework\Filesystem\Driver\File $file,
		\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\File\Csv $csv,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->resultPageFactory = $resultPageFactory;
		$this->storeManager = $storeManager;
		$this->file = $file;
		$this->csv = $csv;
		$this->logger = $logger;
		$this->categoryFactory = $categoryFactory;
		parent::__construct($context);
	}

	public function execute() {
		echo "<pre>";
		print_r($this->readCsv());
		die("====");
		return $this->resultPageFactory->create();
	}
	public function readCsv() {

		$csvFile = '/var/www/magento/var/catwithid3.csv'; //Your CSV file path
		try {
			if ($this->file->isExists($csvFile)) {
				//set delimiter, for tab pass "\t"
				$this->csv->setDelimiter(",");
				//get data as an array
				$data = $this->csv->getData($csvFile);
				if (!empty($data)) {
					// ignore first header column and read data
					foreach (array_slice($data, 1) as $key => $value) {
						echo $id = trim($value['0']);
						echo "->";
						echo $categoryName = trim($value['1']);
						echo "->";
						echo $parent_id = trim($value['2']);
						echo "<br />";
						//and so on.
						$this->fetchOrCreateProductCategory($categoryName, $parent_id);
					}
				}
			} else {
				$this->_logger->info('Csv file not exist');
				return __('Csv file not exist');
			}
		} catch (FileSystemException $e) {
			$this->_logger->info($e->getMessage());
		}
	}
	public function fetchOrCreateProductCategory($categoryName, $parentId) {
		// get the current stores root category

		$parentCategory = $this->categoryFactory->create()->load($parentId);

		$category = $this->categoryFactory->create();
		$cate = $category->getCollection()
			->addAttributeToFilter('name', $categoryName)
			->getFirstItem();

		if (!$cate->getId()) {
			$category->setPath($parentCategory->getPath())
				->setParentId($parentId)
				->setName($categoryName)
				->setIsActive(true);
			$category->save();
		}

		return $category;
	}
	public function getCsvData() {
		$data = [];
		$csvFile = '/var/www/magento/var/catwithid3.csv'; //Your CSV file path
		try {
			if ($this->file->isExists($csvFile)) {
				$this->csv->setDelimiter(",");
				$data = $this->csv->getDataPairs($csvFile);
				return $data;
			} else {
				$this->logger->info('csv file does not exist');
				return false;
			}
		} catch (FileSystemException $e) {
			$this->logger->info($e->getMessage());
			return false;
		}
	}

}
